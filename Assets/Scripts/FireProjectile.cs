using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ShootEventsSubscriber : MonoBehaviour
{
    private void Start()
    {
        ShootEvents shootEvents = GetComponent<ShootEvents>();
        shootEvents.OnSpacePressed += ShootEvents_OnSpacePressed;
    }

    private void ShootEvents_OnSpacePressed(object sender, EventArgs e)
    {
        Debug.Log("Space!");
    }
}
