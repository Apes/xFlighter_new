using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projektil : MonoBehaviour
{

    public float bullet_speed = 30;
    private double timeRemaining = 5;
    private int rotated = 0;
    private Rigidbody rb;
    
    // Start is called before the first frame update
    void Start()
    {
        
        rb=GetComponent<Rigidbody>();
    }

    public GameObject explosion;
    void OnCollisionEnter()
    {
        Instantiate(explosion, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        deleteDelay();
        if(rotated==0){
            transform.rotation = GameObject.Find("Flugzeug P1").transform.rotation;
            rotated=1;
        } 
        transform.position+=transform.forward*Time.deltaTime*bullet_speed;
        
    }

    private void deleteDelay(){
        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
        }
        else
        {
            Destroy(gameObject);
        }
    }

}