using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waffe : MonoBehaviour
{

    public GameObject bullet;
    public double delay = 0.1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (delay > 0)
        {
            delay -= Time.deltaTime;
        }
        else
        {
            if(Input.GetButton("Fire1")) fire(); 
        }
        
    }

    void fire(){

        Instantiate(bullet, transform.position , Quaternion.identity);
        delay=0.1;
        
    }
}
