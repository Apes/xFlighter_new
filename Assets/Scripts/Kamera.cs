using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kamera : MonoBehaviour
{
    // GameObjects
    public GameObject flugzeug;

    // Inputs
    private float mouse3;


    // Offsets
    public float cam_min = -2f;
    public float cam_offset = 2.5f;
    public float cam_max = 15f;
    public float scroll_multiplier = 0.5f;
    void Start()
    {
        
    }

    void Update()
    {
        mouse3 = Input.GetAxis("Mouse ScrollWheel") * scroll_multiplier;
        if(mouse3 != 0) {
            Debug.Log(mouse3);
            cam_offset += mouse3;
            if(cam_offset < cam_min)
                cam_offset = cam_min;
            if(cam_offset > cam_max)
                cam_offset = cam_max;
        }
        this.transform.position = flugzeug.transform.position + new Vector3(0, 0, 0);
        this.transform.rotation = flugzeug.transform.rotation;
    }
}
