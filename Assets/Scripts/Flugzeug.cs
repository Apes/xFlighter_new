using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Flugzeug : MonoBehaviour
{
    private Vector3 facing;

    public GameObject flugzeug;
    private Rigidbody rb;

     /*Inputs*/
    private float verticalInput;
    private float horizontalInput;
    private float Z_Rotation_Input;
    private float Y_Rotation_Input;
    private float X_Rotation_Input;


    /*Movement*/
    public float speed = 0f;
    public float max_speed = 40f;
    public float speed_multiplier = 0.001f;
    public float x_agility = 2f;
    public float y_agility = 1f;
    public float z_agility = 1f;


    /*Rotation Reset Timer*/
   // private float standbyTime = 5f;
    public float resetSpeed = 0.7f;


    /*Aerodynamic*/
    public float air_density = 1.2f;
    public float wing_area = 16f;


    /*
    W,S  (Vertical Input) : Beschleunigen : Speed
    A,D (Horizontal Input) : Nach links und recht lenken : Y-Rotation

    NUM 5, NUM 8 : Nach oben und unten neigen : X-Rotation
    NUM 4, NUM 6 : Nach links und rechts neigen : Z-Rotation
    */

    void Start()
    {
        rb = flugzeug.GetComponent<Rigidbody>();

        // Stuff damit die Kamera passt
        // facing=rb.transform.forward;
        // rb.velocity = facing;
    }

    void FixedUpdate() {

        
        // Geschwindigkeit
        verticalInput = Input.GetAxisRaw("Vertical1");
        if(verticalInput != 0) {
            speed += verticalInput*speed_multiplier;
            if(speed<0)
                speed = 0;
            if(speed>max_speed)
                speed = max_speed;
        }
        flugzeug.transform.position += transform.forward * speed;
        
        //rb.AddForce(Vector3.down * 5 * rb.mass, ForceMode.Force);


        // Auftrieb 

        /**(funktioniert noch nicht, die Formel ist aus WikiPedia, Ich habe Probleme die richtige Funktion und den richtigen Vektor zu finden)*/
        //rb.AddRelativeForce(0, 0, (air_density/2) * wing_area * speed, ForceMode.Force);
        //Debug.Log(rb.velocity.y);

        // Y-Rotation - Links und Rechts lenken
        horizontalInput = Input.GetAxisRaw("Horizontal1");
        if(horizontalInput != 0) {
            float y_rotation = horizontalInput * y_agility;
            flugzeug.transform.Rotate(0, y_rotation, 0, Space.Self);
        }

        // X-Rotation - Nach oben und unten neigen
        X_Rotation_Input = Input.GetAxisRaw("X_Rotation1");
        if(X_Rotation_Input != 0) {
            float x_rotation = X_Rotation_Input * x_agility;
            flugzeug.transform.Rotate(x_rotation, 0, 0, Space.Self);
        }

        // Z-Rotation - Nach links und rechts neigen
        Z_Rotation_Input = Input.GetAxisRaw("Z_Rotation1");
        if(Z_Rotation_Input != 0) {
            float z_rotation = Z_Rotation_Input * z_agility;
            flugzeug.transform.Rotate(0, 0, z_rotation, Space.Self);
        }
        
        // if(horizontalInput+X_Rotation_Input+Z_Rotation_Input == 0) {
        //     standbyTime -= Time.deltaTime;
        //     if(standbyTime <= 0) {
        //         float x_rotation = this.transform.localRotation.eulerAngles.x;
        //         float y_rotation = this.transform.localRotation.eulerAngles.y;
        //         float z_rotation = this.transform.localRotation.eulerAngles.z;
        //         Debug.Log(z_rotation);
        //         if (x_rotation >= 0)
        //             this.transform.Rotate(-resetSpeed, 0, 0, Space.Self);
        //         if (x_rotation <= 0)
        //             this.transform.Rotate(resetSpeed, 0, 0, Space.Self);
        //         if (y_rotation >= 0)
        //             this.transform.Rotate(0, -resetSpeed, 0, Space.Self);
        //         if (y_rotation <= 0)
        //             this.transform.Rotate(0, resetSpeed, 0, Space.Self);
        //         if (z_rotation >= 0)
        //             this.transform.Rotate(0, 0, -resetSpeed, Space.Self);
        //         if (z_rotation <= 0)
        //             this.transform.Rotate(0, 0, resetSpeed, Space.Self);
                
        //     }
        // } else {
        //     standbyTime = 5f;
        // }

    }
}
public class ShootEvents : MonoBehaviour
{
    public EventHandler OnSpacePressed;

    public void Start()
    {

    }

    private void Update()
    {
        if (Input.GetAxisRaw("Jump")==1)
        {
            OnSpacePressed?.Invoke(this, EventArgs.Empty);
        }
    }
}


